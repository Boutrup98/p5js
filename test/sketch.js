let power = [];

function setup() {
	createCanvas(windowWidth, windowHeight);
	for (let i = 0; i < 50; i++) {
		power[i] = new Power(random(width), random(height));
	}
}

function mousePressed() {
	power.push(new Power(mouseX, mouseY));
}

function draw() {
	background(255,120);

	for (let i = 0; i < power.length; i++) {
		power[i].mobility();
		power[i].labour();
		for (let j = 0; j < power.length; j++) {
			if (i != j && power[i].exchange(power[j])) {
				if (power[i].size < power[j].size) {
					power[j].size += 0.01;
					power[i].size -= 0.01;
				} else if (power[i].size > power[j].size) {
					power[i].size += 0.01;
					power[j].size -= 0.01;
				}
				if (power[i].size > width){
					noLoop();
					console.log('STOP');
				}
				if (power[j].size > width){
					noLoop();
					console.log('STOP');
				}
			}
		}
	}
}
